<?php
$library_directory = __DIR__.'/libraries';

// Add a comment
include_once($library_directory.'/library.php');
include_once($library_directory.'/boost.php');

$_ENV['boost_obj'] = new Boost\Boost();

function boost() {

	return $_ENV['boost_obj'];

}

boost()->add_app(
	array(
		'key' => 'default',
		'path' => ROOTPATH.'/'.APPDIR
	)
);

// Require collection first
include_once($library_directory.'/collection.php');

// Include all classes
foreach (glob($library_directory.'/*.php') as $filename) {
	include_once($filename);
}

boost()->is_adding_core_classes = false;

function boost_exception_handler($exception) {
	$exception->throw = true;

	boost()->hook->run('boost-exception-handle-before', $exception);

	$trace = $exception->getTrace();

	if ($exception->throw && count($trace)) {
		$file = empty($trace[0]['file']) ? $exception->getFile() : $trace[0]['file'];
		$line = empty($trace[0]['line']) ? $exception->getline() : $trace[0]['line'];
		echo "<div>";
			echo "<h1>Exception</h1>";
			echo "<h3>Message: ".$exception->getMessage()."</h3>";
			echo "<p>File: ".$file."</p>";
			echo "<p>Line: ".$line."</p>";
		echo "</div>";
	}

	boost()->hook->run('boost-exception-handle-after', $exception);
}