<?php
namespace Boost;

boost()->add_callable('template', 'Boost\Template');

Class Template extends Library {
	function render($template_name = null, $view_name = null, $values = null) {
		echo $this->get($template_name, $view_name, $values);
	}
	function get($template_name = null, $view_name = null, $values = null) {

		$view_contents = boost()->view->get($view_name, $values);

		$template_contents = boost()->view->get($template_name, array(
			'contents' => $view_contents
		), true);

		return $template_contents;

	}
}