<?php
namespace Boost;

boost()->add_callable('css', 'Boost\Css');

Class Css extends Library {
	function load($path = null) {
		boost()->asset->add('css', $path, false);
	}
	function output() {
		$paths = boost()->asset->get_paths('css');
		foreach ($paths AS $path) {
			echo '<link type="text/css" href="'.$path.'" rel="stylesheet" />';
		}
	}
}