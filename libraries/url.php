<?php
namespace Boost;

boost()->add_callable('url', 'Boost\Url', true);

Class Url extends Library {
	public $domain;
	public $domain_parts;
	public $domain_apex;
	public $domain_sub;
	public $request_uri;
	public $request_uri_segments;
	public $request_query_string;
	public $request_method;

	function ___boost_init() {
		// DOMAIN
		$this->domain = $_SERVER['HTTP_HOST'];
		$this->domain_parts = explode('.', $this->domain);
		$this->domain_apex = implode('.', array_slice($this->domain_parts, -2));
		$this->domain_sub = trim(str_replace($this->domain_apex, "", $this->domain), '.');

		// REQUEST URI
		$this->request_uri = $_SERVER['REQUEST_URI'];

		$this->request_uri_segments = $this->get_segments_from_string($this->request_uri);

		$this->request_query_string = strtoupper($_SERVER['QUERY_STRING']);
		$this->request_method = strtoupper($_SERVER['REQUEST_METHOD']);
	}

	function get_request_uri_without_base() {
		$request_uri_without_base = $this->request_uri;

		if (boost()->config->get('site_url_base')) {
			$request_uri_without_base = preg_replace("#^(/)?".boost()->config->get('site_url_base')."#", "", $request_uri_without_base);
		}

		return $request_uri_without_base;
	}

	function get_segments_from_string($string = null) {

		$segments = array();

		$tmp_request_uri = explode("?", $string);
		$tmp_request_uri = $tmp_request_uri[0];

		foreach (explode('/', trim($tmp_request_uri, '/')) AS $tmp_segment) {

			$tmp_segment = trim($tmp_segment);
			if (empty($tmp_segment)) {
				continue;
			}
			$segments[] = $tmp_segment;

		}

		return $segments;

	}

	function full_url() {
		return 'http'.($this->is_secure()?'s':'').'://'.$this->domain.$this->request_uri;
	}

	function segment($segment_number = null) {

		$segment_value = false;
		$segments = $this->segments();

		// If requesting a specific index and that index exists
		if (!empty($segment_number) && !empty($segments[$segment_number-1])) {
			// Return that index
			$segment_value = $segments[$segment_number-1];
		}

		return $segment_value;

	}

	function segments() {
		$segments = $this->request_uri_segments;
		if (boost()->config->get('site_url_base')) {
			$site_url_base_segments = $this->get_segments_from_string(boost()->config->get('site_url_base'));
			$segments = array_slice($segments, count($site_url_base_segments));
		}
		return $segments;
	}

	function throw_404($kill = true) {
		header('HTTP/1.0 404 Not Found');
		if (array_key_exists('404', boost()->route->error_routes)) {
			boost()->route->run_routes(boost()->route->error_routes[404]);
		}
		else {
			echo "Page Not Found";
		}
		if ($kill) {
			exit;
		}
	}
	function img_url($url) {
		$url = trim($url, '/');
		return $this->asset_url('/img/'.$url);
	}

	function asset_url($url = null, $type = 'relative') {
		$url = trim($url, '/');
		$asset_path_relative = preg_replace("#".ROOTPATH."#", "", boost()->get_asset_path());
		$url = $asset_path_relative.'/'.$url;
		return $this->site_url($url, $type, false);
	}

	function site_url($url = null, $type = 'relative', $add_base = true) {
		$url = trim($url, '/');
		$url = '/'.$url;

		$tmp_base = trim(boost()->config->get('site_url_base'), '/');

		if ($add_base && $tmp_base) {
			$url = '/'.$tmp_base.$url;
		}

		switch ($type) {
			case 'relative':
				// Nothing required
				break;
			case 'absolute':
				// Add http, etc
				$url = 'http://'.$this->domain.$url;
				break;
			case 'local':
			case 'system':
				// Add system path
				$url = ROOTPATH.$url;
				break;
		}

		return $url;
	}

	function is_secure() {
		return !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443;
	}
}