<?php
namespace Boost;

boost()->add_callable('view', 'Boost\View');

Class View extends Library {

	function render($view_name = null, $values = null) {
		echo $this->get($view_name, $values);
	}

	function get($boost_tmp_view_names = null, $boost_tmp_values_to_pass = array(), $is_template = false) {

		if (!empty($boost_tmp_view_names)) {

			if (!is_array($boost_tmp_view_names)) {
				$boost_tmp_view_names = array($boost_tmp_view_names);
			}

			$boost_tmp_global_values = boost()->value->get();

			ob_start();

			if ($is_template) {
				$folder_path = boost()->get_template_path();
				boost()->hook->run('boost-template-path', $folder_path);
			}
			else {
				$folder_path = boost()->get_view_path();
				boost()->hook->run('boost-view-path', $folder_path);
			}

			foreach ($boost_tmp_view_names AS $boost_tmp_view_name) {

				$boost_tmp_file = $folder_path.'/'.$boost_tmp_view_name.'.php';

				if (!file_exists($boost_tmp_file)) {
					throw new Exception('File does not exist: '.$boost_tmp_file);
				}

				if (is_array($boost_tmp_values_to_pass)) {
					extract($boost_tmp_values_to_pass, EXTR_OVERWRITE);
				}
				extract($boost_tmp_global_values, EXTR_OVERWRITE);

				require($boost_tmp_file);

			}

			$boost_tmp_contents = ob_get_contents();

			ob_end_clean();

			return $boost_tmp_contents;

		}

	}
}