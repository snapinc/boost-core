<?php
namespace Boost;

boost()->add_callable('hook', 'Boost\Hook');

Class Hook extends Library {
	public $hooks = array();

	function run($hook_name = null, &$incoming_value = null) {
		if (!empty($hook_name) && array_key_exists($hook_name, $this->hooks)) {
			$matching_hooks = $this->hooks[$hook_name];
			krsort($matching_hooks);
			foreach ($matching_hooks AS $priority_hash => $closure) {
				$returned_val = call_user_func($closure, $incoming_value);
				if (isset($returned_val)) {
					$incoming_value = $returned_val;
				}
			}
		}
	}

	function into($hook_name = null, $closure = null, $priority = 0.00) {
		if (!empty($hook_name) && !empty($closure) && is_callable($closure)) {
			$priority_hash = $priority.'-'.uniqid();
			$this->hooks[$hook_name][$priority_hash] = $closure;
		}
	}
}