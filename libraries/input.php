<?php
namespace Boost;

boost()->add_callable('input', 'Boost\Input', true);

Class Input extends Library {
	public $post;
	public $get;

	function ___boost_init() {
		$this->post = boost()->collection->create()->fill($_POST);
		$this->get = boost()->collection->create()->fill($_GET);
	}

	function post($key = null) {
		return $this->post->get($key);
	}

	function get($key = null) {
		return $this->get->get($key);
	}
}