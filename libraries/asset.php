<?php
namespace Boost;

boost()->add_callable('asset', 'Boost\Asset');

Class Asset extends Library {
	private $stored = array();

	private function correct_link($type = null, $path = null, $auto_folder_name = false) {
		if (!$this->is_remote($path)) {
			if ($auto_folder_name) {
				$path = $type.'/'.$path;
			}
			$path = boost()->url->asset_url($path);
		}
		return $path;
	}

	private function add_timestamp($path = null) {
		if (!$this->is_remote($path)) {
			$path .= '?ts='.@filemtime(ROOTPATH.$path);
		}
		return $path;
	}

	private function is_remote($path) {
		return preg_match('#^http|^//#i', $path);
	}

	function add($type = null, $path = null, $auto_folder_name = false) {
		$path = $this->correct_link($type, $path, $auto_folder_name);
		if (!$this->is_remote($path)) {
			if (!file_exists(ROOTPATH.$path)) {
				return false;
			}
		}
		$path = $this->add_timestamp($path);
		$this->stored[$type][] = $path;
	}

	function get_paths($type = null, $force_exists = true) {
		return empty($this->stored[$type]) ? array() : $this->stored[$type];
	}

	function load($path, $type = null) {
		$auto_folder_name = false;
		if (empty($type)) {
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$auto_folder_name = true;
		}
		$this->add($type, $path, $auto_folder_name);
	}
}