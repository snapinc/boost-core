<?php
namespace Boost;

boost()->add_callable('js', 'Boost\Js');

Class Js extends Library {
	function load($path = null) {
		boost()->asset->add('js', $path, false);
	}
	function output() {
		$paths = boost()->asset->get_paths('js');
		foreach ($paths AS $path) {
			echo '<script type="text/javascript" src="'.$path.'"></script>';
		}
	}
}