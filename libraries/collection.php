<?php
namespace Boost;

boost()->add_callable('collection', 'Boost\Collection');

Class Collection extends Library {
	public $values = array();

	function set($name = null, $value = null) {
		boost()->utility->set_array_value($name, $value, $this->values);
		return $this;
	}

	function delete($name = null) {
		boost()->utility->unset_array_value($name, $this->values);
		return $this;
	}

	function get($name = null) {
		return boost()->utility->get_array_value($name, $this->values);
	}

	function clear() {
		$this->values = array();
		return $this;
	}

	function prepend($name = null, $value = null) {
		$existing_value = $this->get($name);
		$new_value = $value . $existing_value;
		$this->set($name, $new_value);
		return $this;
	}

	function append($name = null, $value = null) {
		$existing_value = $this->get($name);
		$new_value = $existing_value . $value;
		$this->set($name, $new_value);
		return $this;
	}

	function fill($values = array()) {
		$values = boost()->utility->sanitize_array($values);
		$this->values = $values;
		return $this;
	}
}