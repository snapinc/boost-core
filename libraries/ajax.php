<?php
namespace Boost;

// boost()->add_callable('ajax', 'Boost\Ajax', true);

Class Ajax extends Collection {
	public $errors = array();

	function DISABLED___boost_init() {

		// On all AJAX routes, before other routes
		boost()->route->add(':ajax', function() {

			// Hook into exceptions and make them log an ajax error rather than
			// throwing an error.
			boost()->hook->into('boost-exception-handle-before', function($exception) {
				$exception->throw = false;
				echo json_encode(array(
					'error' => $exception->getMessage()
				));
			});

		}, -1000);

		// On all AJAX calls, after other routes
		boost()->route->add(':ajax', function() {

			// Output JSON of the resulting data, with success = 1
			boost()->ajax->set('success', 1);
			echo json_encode(boost()->ajax->get());

		}, 1000);

	}
}