<?php
namespace Boost;

class Library {
	function create() {
		return new $this;
	}
	function __call($name, $args) {
		if (preg_match("#^app_(.*)#", $name, $matches)) {
			$method_name = $matches[1];
			$app_name = array_shift($args);
			boost()->temp_change_app($app_name);
			$return_val = call_user_func_array(array(get_called_class(), $method_name), $args);
			boost()->restore_previous_app();
			return $return_val;
		}
		throw new \Exception('The method ("'.$name.'") does not exist in the library ("'.get_called_class().'").');
	}
	function printout() {
		boost()->utility->printout($this);
	}
}