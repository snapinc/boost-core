<?php
namespace Boost;

function boost() {
    $args = func_get_args();
    return call_user_func_array('\boost', $args);
}

class Boost extends Library {

	private $callable_classes = array();
	private $called_classes = array();
	private $called_class_instances = array();

	private $initable_classes = array();
	private $inited_classes = array();

	private $active_app_key = null;
	private $initial_app_key = null;
	private $apps = array();
	private $app_chain = array();
	private $is_temporary_app = false;

	public function add_app($app_info = array()) {
		if (empty($app_info['key'])) {
			throw new Exception('add_app: key is required');
		}
		if (empty($app_info['path'])) {
			throw new Exception('add_app: path is required');
		}
		if (!file_exists($app_info['path'])) {
			throw new Exception('add_app: path ('.$app_info['path'].') does not exist');
		}
		$this->apps[$app_info['key']] = $app_info;
		if (empty($this->active_app_key)) {
			$this->change_app($app_info['key']);
		}
	}

	public function get_app($app_key, $key = null) {
		if (!$this->app_exists($app_key)) {
			throw new Exception('app ('.$app_key.') does not exists');
		}
		$return_val = $this->apps[$app_key];
		if ($key) {
			$return_val = array_key_exists($key, $return_val) ? $return_val[$key] : null;
		}
		return $return_val;
	}

	public function get_active_app($key = null) {
		return $this->get_app($this->active_app_key, $key);
	}

	public function app_exists($app_key) {
		return array_key_exists($app_key, $this->apps);
	}

	public function change_app($app_key, $ignore_chain = false) {
		$app = $this->get_app($app_key);
		if (empty($this->initial_app_key)) {
			$this->initial_app_key = $app_key;
		}
		if ($this->active_app_key && !$ignore_chain) {
			$this->app_chain[] = $this->active_app_key;
		}
		$this->active_app_key = $app['key'];
	}

	public function restore_initial_app() {
		$this->change_app($this->initial_app_key, true);
		$this->app_chain = array();
	}

	public function add_callable($name, $class, $has_init = false) {
		$this->callable_classes[$name] = $class;
		if ($has_init) {
			$this->initable_classes[] = $name;
		}
	}

	public function restore_previous_app() {
		if ($this->is_temporary_app) {
			$last_app = array_pop($this->app_chain);
			if ($last_app) {
				$this->change_app($last_app, true);
			}
			$this->is_temporary_app = false;
		}
	}

	public function temp_change_app($app_key) {
		$this->change_app($app_key);
		$this->is_temporary_app = true;
	}

	public function __get($name) {

		if (array_key_exists($name, $this->callable_classes)) {
			if (!array_key_exists($name, $this->called_classes)) {
				$this->called_classes[$name] = new $this->callable_classes[$name]();

				if (!in_array($name, $this->inited_classes) && method_exists($this->called_classes[$name], '___boost_init')) {
					$this->called_classes[$name]->___boost_init();
				}

				if (method_exists($this->called_classes[$name], '___boost_instance')) {
					$this->called_class_instances[$name] = $this->called_classes[$name]->___boost_instance();
				}
			}
			return array_key_exists($name, $this->called_class_instances) ? $this->called_class_instances[$name] : $this->called_classes[$name];
		}
		throw new \Exception('The library ('.$name.') is not callable.');

	}

	public function init_callables() {

		foreach ($this->initable_classes AS $initable_class) {
			boost()->$initable_class;
		}

	}

	public function get_app_path() {
		$path = $this->get_active_app('path');
		boost()->hook->run('boost-app-path', $path);
		return $path;
	}
	public function get_template_path() {
		$path = $this->get_app_path().'/'.TEMPLATEDIR;
		boost()->hook->run('boost-template-path', $path);
		return $path;
	}
	public function get_view_path() {
		$path = $this->get_app_path().'/'.VIEWDIR;
		boost()->hook->run('boost-view-path', $path);
		return $path;
	}
	public function get_asset_path() {
		$path = $this->get_app_path().'/'.ASSETDIR;
		boost()->hook->run('boost-asset-path', $path);
		return $path;
	}
}