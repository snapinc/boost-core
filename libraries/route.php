<?php
namespace Boost;

boost()->add_callable('route', 'Boost\Route');

Class Route extends Library {
	public $routes = array();
	public $matching_routes = null;
	public $num_matching_primary = 0;
	public $stored_routes = array();
	public $error_routes = array();

	private $group_strings = array();

	function store($name = null, $closure = null) {
		if (!empty($name) && !empty($closure) && is_callable($closure)) {
			$this->stored_routes[$name] = $closure;
		}
	}

	function run($name = null, $arguments = array()) {
		$return_val = null;
		if (!empty($name) && array_key_exists($name, $this->stored_routes)) {
			$closure = $this->stored_routes[$name];
			$return_val = call_user_func_array($closure, $arguments);
		}
		return $return_val;
	}

	function add($string = null, $closure = null, $priority = 0) {
		if (empty($string)) {
			$string = '*';
		}
		if (!empty($closure) && is_callable($closure)) {
			$string = $this->combine_route_strings($this->last_group_string(), $string);
			$this->routes[$this->generate_priority_string($priority)] = array(
				'string' => $string,
				'exploded' => $this->explode_string($string),
				'closure' => $closure,
				'priority' => $priority
			);
			$this->sort_routes();
		}
	}

	private function clean_string($string = "") {
		if (empty($string)) {
			$string = '*';
		}

		$string = str_replace("^/", "^/?", $string);
		$string = str_replace("/$", "/?$", $string);

		switch ($string) {
			case '*':
				$string = '.*';
				break;
			case '/':
				$string = '^/?$';
				break;
		}

		return $string;
	}

	private function explode_string($string = "") {
		$requirements = array();
		$actual_route = "";

		$counter = 0;
		foreach (explode(':', $string) AS $requirement) {
			if ($counter) {
				$requirements[] = $requirement;
			}
			else {
				$actual_route = $requirement;
			}
			$counter++;
		}

		return array(
			'route' => $this->clean_string($actual_route),
			'requirements' => $requirements
		);
	}

	function group($string = null, $closure = null) {
		if (empty($string)) {
			$string = '*';
		}
		if (!empty($closure) && is_callable($closure)) {
			$this->group_strings[] = $this->combine_route_strings($this->last_group_string(), $string);
			$closure();
			array_pop($this->group_strings);
			$this->sort_routes();
		}
	}

	private function last_group_string() {
		return empty($this->group_strings) ? "" : $this->group_strings[count($this->group_strings)-1];
	}

	private function combine_route_strings($string_1 = "", $string_2 = "") {

		$string_1 = $this->explode_string($string_1);
		$string_2 = $this->explode_string($string_2);

		// if (empty($string_1) || empty($string_2)) {
		// 	return $string_1.$string_2;
		// }

		if ($string_1['route'] == '.*' && $string_2['route'] == '.*') {
			$string_2['route'] = '';
		}
		elseif ($string_1['route'] == '.*' && $string_2['route'] != '.*') {
			$string_1['route'] = '';
		}
		elseif ($string_2['route'] == '.*' && $string_1['route'] != '.*') {
			$string_2['route'] = '';
		}

		$string_base = $string_1['route'];

		if (!empty($string_2['route'])) {
			if (empty($string_base)) {
				$string_base = $string_2['route'];
			}
			else {
				$string_base = $string_base.'/'.$string_2['route'];
			}
		}

		$requirements = array();

		$string_parts_combined = array_merge($string_1['requirements'], $string_2['requirements']);

		foreach ($string_parts_combined AS $k => $v) {
			$v = strtoupper($v);
			$requirements[$v] = 1;
		}

		$final_string = $string_base;

		if (count($requirements)) {
			$final_string .= ':'.implode(':', array_keys($requirements));
		}

		return $final_string;
	}

	private function sort_routes() {
		ksort($this->routes);
	}

	private function generate_priority_string($priority = 0) {
		$priority = number_format($priority, 3, '.', '');
		return $priority."_".uniqid();
	}

	function num_matching_routes() {
		if ($this->matching_routes == null) {
			$this->determine_matching();
		}
		return count($this->matching_routes);
	}

	function run_routes($routes = array(), $require_primary = false) {
		foreach ($routes AS $route) {
			if (!$require_primary || ($route['type'] == 'primary' || $this->num_matching_primary)) {
				call_user_func_array($route['closure'], $route['arguments']);
			}
		}
	}

	function run_matching() {
		// IF NO MATCHING ROUTES, THROW 404
		if (!$this->num_matching_primary) {
			boost()->url->throw_404();
		}
		$this->run_routes($this->matching_routes, true);
	}

	function determine_matching() {
		$this->matching_routes = array();

		// RUN MATCHING ROUTES
		foreach ($this->routes AS $route) {

			$meets_requirements = true;
			$allowed_methods = array();
			$is_primary = true;

			$request_uri_without_base = trim(boost()->url->get_request_uri_without_base(), "/");
			if ($request_uri_without_base == "") {
				$request_uri_without_base = "/";
			}

			if (!preg_match("#".$route['exploded']['route']."#", $request_uri_without_base, $arguments_for_closure)) {
				continue;
			}

			array_shift($arguments_for_closure);

			$tmp_this_route = array(
				'type' => $is_primary ? 'primary' : 'secondary',
				'closure' => $route['closure'],
				'arguments' => $arguments_for_closure
			);

			// SEE IF REQUIREMENTS ARE MET
			foreach ($route['exploded']['requirements'] AS $requirement) {
				$requirement = strtoupper($requirement);

				switch ($requirement) {
					case 'HTTP':
						if (boost()->url->is_secure()) {
							$meets_requirements = false;
						}
						break;
					case 'HTTPS':
						if (!boost()->url->is_secure()) {
							$meets_requirements = false;
						}
						break;
					case 'GET':
					case 'POST':
						$allowed_methods[] = $requirement;
						break;
					case 'AJAX':
						if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
							strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
							$meets_requirements = false;
						}
						break;
					case 'DEPENDENT':
						$is_primary = false;
						break;
					case '404':
						$this->error_routes[404][] = $tmp_this_route;
						continue(3);
						break;
				}

				// Domain?
				if (preg_match("#DOMAIN\((.*)\)#", $requirement, $matches)) {
					$domain = $this->clean_string($matches[1]);
					if (!preg_match("#".$domain."#i", boost()->url->domain)) {
						$meets_requirements = false;
					}
				}
			}

			// If requiring a method, make sure it is in use
			if (!empty($allowed_methods) && !in_array(boost()->url->request_method, $allowed_methods)) {
				$meets_requirements = false;
			}

			// Didn't meet requirements?
			if (!$meets_requirements) {
				continue;
			}

			if ($is_primary) {
				$this->num_matching_primary++;
			}

			$this->matching_routes[] = $tmp_this_route;

		}

	}
}