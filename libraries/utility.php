<?php
namespace Boost;

boost()->add_callable('utility', 'Boost\Utility');

Class Utility extends Library {

	function sanitize_array($array = null) {

		foreach ($array as $key => $val) {
			if (is_array($val)) {
				$val = $this->sanitize_array($val);
			} else {
				if ($val=='' || trim($val)=='') {
					$val = null;
				}
			}
			$array[$key] = $val;
		}

		return $array;

	}

	/* Recursively merge arrays such that keys are never deleted, but combined */
	function recursively_merge_arrays($array1 = array(), $array2 = array()) {

		$return = false;

		// Make sure they are both arrays
		if (is_array($array1) && is_array($array2)) {
			// For each key/value in array two
			foreach ($array2 AS $k => $v) {
				// If the key is an integer
				if (is_integer($k)) {
					// Append the value to array 1, disregarding the key
					$array1[] = $v;
				}
				// If the key is not an integer
				else {
					// If the key already exists in array 1
					if (array_key_exists($k, $array1)) {
						// If both values in both arrays are arrays themselves
						if (is_array($array1[$k]) && is_array($array2[$k])) {
							// Then we need to merge them together via this function
							$array1[$k] = $this->recursively_merge_arrays($array1[$k], $array2[$k]);
						}
						// Otherwise, if array 2's value is an array
						elseif (is_array($array2[$k])) {
							// Make array 1's value into an array
							$array1[$k] = array($array1[$k]);
							// Merge them together
							$array1[$k] = $this->recursively_merge_arrays($array1[$k], $array2[$k]);
						}
						// Otherwise, if array 1's value is an array
						elseif (is_array($array2[$k])) {
							// Make array 2's value into an array
							$array2[$k] = array($array2[$k]);
							// Merge them together
							$array1[$k] = $this->recursively_merge_arrays($array1[$k], $array2[$k]);
						}
						// Otherwise, if neither are arrays, then replace array1 value with array2
						else {
							$array1[$k] = $array2[$k];
						}
					}
					// Otherwise, if array 2's key is new to array 1, simply add it to array 1
					else {
						$array1[$k] = $array2[$k];
					}
				}
			}

			// Return array 1 (since it has been modified by array 2)
			$return = $array1;
		}

		return $return;

	}

	/* Sets value by recursing through an array */
	function set_array_value($item_key = null, $item_value = null, &$items) {

		if (!is_array($items)) {
			$items = array();
		}

		if (is_array($item_key)) {

			if ($this->is_assoc($item_key)) {
				foreach ($item_key AS $k => $v) {
					$this->set_array_value($k, $v, $items);
				}
			}
			else {
				for ($i = 0; $i < count($item_key); $i++) {
					// If Its The Value You Want To Set
					if ($i == count($item_key) - 1) {
						$items[$item_key[$i]] = $item_value;
					}
					else{
						// If It Doesn't Exist, Create It
						if (!isset($items[$item_key[$i]])) {
							$items[$item_key[$i]] = array();
						}

						$items =& $items[$item_key[$i]];
					}
				}
			}
		}
		else {
			$items[$item_key] = $item_value;
		}

	}

	/* Unsets value by recursing through an array */
	function unset_array_value($key, &$items){

		if(is_array($key)){
			for($i = 0; $i < count($key); $i++){
				if($i == (count($key) - 1)) {
					unset($items[$key[$i]]);
				} else {
					$items =& $items[$key[$i]];
				}
			}
		} else {
			unset($items[$key]);
		}

	}

	/* Checks/returns if an array is associative */
	function is_assoc($array = array()){

		return array_keys($array) !== range(0, (count($array) - 1));

	}

	/* Only returns vals whose keys are present in the $keys array */
	function filter_keys($vals = array(), $keys = array()) {

		$good_vals = array();

		foreach ($vals AS $k => $v) {
			if (in_array($k, $keys)) {
				$good_vals[$k] = $v;
			}
		}

		return $good_vals;

	}

	/* Gets value by recursing through an array */
	function get_array_value($item_key = null, $items = array()) {

		$value = null;

		// If they key we want is null, then return the entire array
		if (empty($item_key)) {
			$value = $items;
		}
		// Otherwise, if the key is not null
		else {
			// If the key is an array
			if (is_array($item_key)) {
				$array = $items;

				// Navigate through the array, and find the final element in the key.
				// If the key does not exist at any time, return false
				for ($i = 0; $i < count($item_key); $i++) {
					if (!isset($array[$item_key[$i]])) {
						break;
					}
					elseif ($i == count($item_key) - 1) {
						$value = $array[$item_key[$i]];
					}
					else{
						$array = $array[$item_key[$i]];
					}
				}
			}
			// If it's not an array, but it exists, then return it
			elseif (isset($items[$item_key])) {
				$value = $items[$item_key];
			}
		}

		return $value;

	}

	function get_vars_from_file($boost_tmp_var_file = null) {

		require($boost_tmp_var_file);
		unset($boost_tmp_var_file);

		$defined_vars = get_defined_vars();
		$vars = array();

		if ($defined_vars) {
			foreach ($defined_vars AS $k => $v) {
				$vars[$k] = $v;
			}
		}

		return $vars;

	}

	function include_file_or_folder($location = null, $name = null) {
		$return = null;
		if (file_exists($location.'/'.$name.'.php')) {
			require_once($location.'/'.$name.'.php');
		}
		elseif (file_exists($location.'/'.$name.'/')) {
			foreach (glob($location.'/'.$name.'/*.php') AS $tmp_file) {
				require_once($tmp_file);
			}
		}
		return $return;
	}

	function printout($content = null) {
		echo "<div><pre>";
		print_r($content);
		echo "</pre></div>";
	}

	function require_values($values = array(), $requirements = array()) {
		foreach ($requirements AS $field_name => $label) {
			if (empty($values[$field_name])) {
				throw new Exception($label.' is required.');
			}
		}
	}
}